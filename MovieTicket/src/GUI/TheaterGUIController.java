package GUI;

import java.util.Arrays;
import dataAccess.*;
import dataStructure.*;

/**
 * TheaterGUIController class contain methods gathering information for GUI 
 * 
 * @author B01801036
 * @version 6.0
 * @since 2017-06-12
 */

public class TheaterGUIController {
	
	/**
	 * theater with specific hall and seat 
	 */
	Theater t = new Theater();
	
	/**
	 * system to book and cancel ticket 
	 */
	TicketSystem systemA = new TicketSystem();
	
	/**
	 * system to search ticket, movies, and seats 
	 */
	Movie_DAO systemB = new Movie_DAO();
	
	/**
	 * TheaterGUIController can create a theater GUI controller  
	 */
	public TheaterGUIController() {
		
	}

	/**
	 * getAllUserID method can return all user IDs
	 * 
	 * @return all user IDs
	 */
	public String[] getAllUserID() {
		String[] userID = new String[t.userID.length];
		for (int i = 0; i < t.userID.length; i++) {
			userID[i] = Integer.toString(t.userID[i]);	
		}
		return userID;
	}
	
	/**
	 * getAllMovieNames method can return all movie names in this theater
	 * 
	 * @return all movie names
	 */
	public String[] getAllMovieNames() {
		return Movie.getAllMovieNames();
	}
	
	/**
	 * getMovieTime method can return all the play time of a specific movie   
	 * 
	 * @param movieID
	 *            movie ID  
	 *            
	 * @return all the play time of a specific movie
	 */
	public String[] getMovieTime(int movieID) {
		String movieTimeInfo = Movie.getMovieTime(movieID);
		String[] movieTime = movieTimeInfo.split("、");
		return movieTime;
	}
	
	/**
	 * getMovieHall method can return the movie hall of a specific movie  
	 * 
	 * @param movieID
	 *            movie ID  
	 * @return movie hall of a specific movie           
	 */
	public String getMovieHall(int movieID) {
		String movieHall = Movie.getMovieHall(movieID);
		return movieHall;
	}
	
	/**
	 * getMovieHallSize method can return the size of the movie hall of a specific movie  
	 * 
	 * @param movieID
	 *            movie ID  
	 * @return size of the movie hall of a sprcific movie
	 */
	public String getMovieHallSize(int movieID) {
		String movieHallSize = t.getHallSize(getMovieHall(movieID)).toString();
		return movieHallSize;
	}
	
	/**
	 * getMovieHallAllRows method can return all the row IDs of the movie hall of a specific movie  
	 * 
	 * @param movieID
	 *            movie ID  
	 * @return all the row IDs of the movie hall of a specific movie 
	 */
	public String[] getMovieHallAllRows(int movieID) {
		String[] movieHallAllRows = t.getHallAllRows(t.getHallSize(getMovieHall(movieID)));
		return movieHallAllRows;
	}
	
	/**
	 * getAllAreas method can return all the area names of a movie hall  
	 * 
	 * @return all the area names of a movie hall 
	 */
	public String[] getAllAreas() {
		String[] allHallAreas = t.getAllAreas();
		return allHallAreas;
	}
	
	/**
	 * getAreaRows method can return all the row IDs of the specific area in the movie hall  
	 * 
	 * @param area
	 *            name of the specific area in the movie hall     
	 * @return all the row IDs of the specific area in the movie hall 
	 */
	public String[] getAreaRows(String area) {
		String[] areaRows = t.getAreaRows(area);
		return areaRows;
	}
	
	/**
	 * book method can book a ticket if possible   
	 * 
	 * @param userID
	 *            user ID     
	 * @param movieID
	 *            movie ID     
	 * @param movieTime
	 *            play time     
	 * @param seatCont
	 *            if limit with continuous seats     
	 * @param seatRow
	 *            if limit with specified row ID     
	 * @param row
	 *            specified row ID     
	 * @param seatArea
	 *            if limit with specified area name     
	 * @param area
	 *            specified area name     
	 * @param ticketNumber
	 *            ticket number to book    
	 * @return booking message
	 * @exception ticketSystemException 
	 */
	public String book(int userID, int movieID, String movieTime, boolean seatCont,
			boolean seatRow, String row, boolean seatArea, String area, String ticketNumber) throws ticketSystemException {
		userID = userID + 1;
		movieID = movieID + 1;
		switch (area) {
		case "精華":
			area = "red";
			break;
		case "最佳":
			area = "yellow";
			break;
		case "次佳":
			area = "blue";
			break;
		default:
			area = "gray";
			break;
		}
		int ticketN = Integer.parseInt(ticketNumber);
		if (seatRow == true && seatArea == true) {
			return systemA.bookTicket(userID, movieID, movieTime, row, area, ticketN, seatCont);
		} else if (seatRow == true && seatArea == false) {
			return systemA.bookTicket(userID, movieID, movieTime, row, null, ticketN, seatCont);
		} else if (seatRow == false && seatArea == true) {
			return systemA.bookTicket(userID, movieID, movieTime, null, area, ticketN, seatCont);
		} else {
			return systemA.bookTicket(userID, movieID, movieTime, null, null, ticketN, seatCont);
		}
	}
	
	/**
	 * cancel method can cancel a specific ticket if possible   
	 * 
	 * @param ticketID
	 *            ticket ID     
	 * @return canceling message
	 * @exception ticketSystemException 
	 */
	public String cancel(String ticketID) throws ticketSystemException{
		return systemA.cancelTicket(ticketID);
	}
	
//	private String[] getTicketInfo(String ticketIDString) {
//		int ticketID = Integer.parseInt(ticketIDString);
//		String movieName = systemB.getMovieTicket(ticketID).getMovie_name();
//		String movieTime = systemB.getMovieTicket(ticketID).getTime().toString();
//		String movieHall = systemB.getMovieTicket(ticketID).getHall();
//		String seatRow = systemB.getMovieTicket(ticketID).getRow();
//		String seatNum = systemB.getMovieTicket(ticketID).getSeatNum();
//		String seat = seatRow + "_" + seatNum;
//		String[] ticketInfo = new String[4];
//		ticketInfo[0] = movieName;
//		ticketInfo[1] = movieTime;
//		ticketInfo[2] = movieHall;
//		ticketInfo[3] = seat;
//		return ticketInfo; 
//	}
	
	/**
	 * getTicketMovieName method can return the name of the movie of a specific ticket   
	 * 
	 * @param ticketID
	 *            ticket ID     
	 * @return name of the movie of a specific ticket
	 */
	public String getTicketMovieName(String ticketIDString) {
		String movieName = systemB.getMovieTicket(ticketIDString).getMovie_name();
		return movieName; 
	}
	
	/**
	 * getTicketMovieTime method can return the play time of the movie of a specific ticket   
	 * 
	 * @param ticketID
	 *            ticket ID     
	 * @return play time of the movie of a specific ticket
	 */
	public String getTicketMovieTime(String ticketIDString) {
		String movieTime = systemB.getMovieTicket(ticketIDString).getTime().toString();
		return movieTime;
	}
	
	/**
	 * getTicketMovieHall method can return the hall of the movie of a specific ticket   
	 * 
	 * @param ticketID
	 *            ticket ID     
	 * @return hall of the movie of a specific ticket
	 */
	public String getTicketMovieHall(String ticketIDString) {
		String movieHall = systemB.getMovieTicket(ticketIDString).getHall();
		return movieHall; 
	}
	
	/**
	 * getTicketSeat method can return the seatID of the movie of a specific ticket   
	 * 
	 * @param ticketID
	 *            ticket ID     
	 * @return seatID of the movie of a specific ticket
	 */
	public String getTicketSeat(String ticketIDString) {
		String seatRow = systemB.getMovieTicket(ticketIDString).getRow();
		String seatNum = systemB.getMovieTicket(ticketIDString).getSeatNum();
		String seat = seatRow + "_" + seatNum;
		return seat; 
	}
	
	/**
	 * getScoredMovies method can return the information of the movies with higher than specific score    
	 * 
	 * @param movieScore
	 *            score     
	 * @return information of the movies with higher than specific score
	 */
	public String getScoredMovies(String movieScore) {
		double score = Double.parseDouble(movieScore);
		ScoreSearchList list = systemB.getScoreMovie(score);
		String scoredMovies = "";
		for (ScoreSearch e: list)
		{
			scoredMovies += e.getMovie() + "\n";
		}
		return scoredMovies;
	}
	
//	public String[] getMovieInfo(int movieID) {
//		movieID = movieID + 1;
//		String movieRate = systemB.getMovieInfo_output(movieID).getClassification();
//		String movieTime = systemB.getMovieInfo_output(movieID).getTime().toString();
//		String movieHall = systemB.getMovieInfo_output(movieID).getHall();
//		String[] movieInfo = new String[3];
//		movieInfo[0] = movieRate;
//		movieInfo[1] = movieTime;
//		movieInfo[2] = movieHall;
//		return movieInfo;
//	}
	
	/**
	 * getSearchMovieRate method can return the rate of a specific movie   
	 * 
	 * @param movieID
	 *            movie ID     
	 * @return the rate of a specific movie
	 */
	public String getSearchMovieRate(int movieID) {
		movieID = movieID + 1;
		String movieRate = systemB.getMovieInfo_output(movieID).getClassification();
		return movieRate;
	}
	
	/**
	 * getSearchMovieTime method can return the play time of a specific movie   
	 * 
	 * @param movieID
	 *            movie ID     
	 * @return the movie time of a specific movie
	 */
	public String getSearchMovieTime(int movieID) {
		movieID = movieID + 1;
		String movieTime = systemB.getMovieInfo_output(movieID).getTime().toString();
		return movieTime;
	}
	
	/**
	 * getSearchMovieHall method can return the hall of a specific movie   
	 * 
	 * @param movieID
	 *            movie ID     
	 * @return the hall of a specific movie
	 */
	public String getSearchMovieHall(int movieID) {
		movieID = movieID + 1;
		String movieHall = systemB.getMovieInfo_output(movieID).getHall();
		return movieHall;
	}
	
	/**
	 * getSeatableMovies method can return the information of the movies with specific seat requirement    
	 * 
	 * @param ticketNumber
	 *            ticket number     
	 * @param earlyTime
	 *            specific time which movie play after it     
	 * @param lateTime
	 *            specific time which movie play before it     
	 * @param shortestTime
	 *            specific time which movie play at least that long     
	 * @param longestTime
	 *            specific time which movie play no more than that long     
	 * @return information of the movies with specific seat requirement
	 * @exception Exception
	 */
	public String getSeatableMovies(String ticketNumber, String earlyTime, String lateTime, String shortestTime, String longestTime) throws Exception {
		int ticketN = Integer.parseInt(ticketNumber);
		int shortest = Integer.parseInt(shortestTime); 
		int longest =  Integer.parseInt(longestTime);
		MoviePlayList list = systemB.getTimeInfor(ticketN, earlyTime, lateTime, longest, shortest);
		String seatableMovies = "";
		for (MoviePlay e: list)
		{
			seatableMovies += e.toString() + System.lineSeparator();
		}
		return seatableMovies;
	}
	
}
