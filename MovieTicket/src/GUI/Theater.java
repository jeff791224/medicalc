package GUI;

/**
 * Theater class contain setting of the hall and seat in a theater 
 * 
 * @author B01801036
 * @version 6.0
 * @since 2017-06-12
 */

public class Theater {

	/**
	 * all the member ID 
	 */
	int[] userID = new int[21]; 
    
	/**
	 * Theater constructor can create a theater 
	 */
	public Theater() {
        for (int i = 0; i < 21; i++) {
            userID[i] = i + 1;
        }
    }
    
	/**
	 * hall size 
	 */
	public enum HallSize {
        LARGE, SMALL
    }
    
	/**
	 * getHallSize method can return the hall size of a specific hall
	 * 
	 * @param movieHall
	 * 			movie hall
	 * @return hall size
	 */
	public HallSize getHallSize(String movieHall) {
        HallSize hallSize;
        switch (movieHall) {
        case "武當":
            hallSize = HallSize.LARGE;
            break;
        case "少林":
            hallSize = HallSize.LARGE;
            break;
        case "華山":
            hallSize = HallSize.LARGE;
            break;
        case "峨嵋 ":
            hallSize = HallSize.SMALL;
            break;
        case "崆峒 ":
            hallSize = HallSize.SMALL;
            break;
        default: 
            hallSize = HallSize.LARGE;
            break;
        }
        return hallSize;
    }
    
	/**
	 *  getHallAllRows method can return all the row IDs of a specific hall size
	 *  
	 *  @param hall size
	 *  @return all the row IDs of a specific hall size
	 */
	public String[] getHallAllRows(HallSize hallSize) {
        String[] seatRows = null;
        switch (hallSize) {
        case LARGE:
            seatRows = "ABCDEFGHIJKLM".split("");
            break;
        case SMALL:
            seatRows = "ABCDEFGHI".split("");
            break;
        }
        return seatRows;
    }
    
	/**
	 * getAllAreas method can return all the names of areas
	 * 
	 *  @return all the names of areas
	 */
	public String[] getAllAreas() {
        String[] seatArea = { "精華", "最佳", "次佳" };
        return seatArea;
    }
    
	/**
	 * getAreaRows method can return all the row IDs of specific areas
	 *
	 *  @param area 
	 *  		specific area
	 *  @return all the row IDs of areas
	 */
	public String[] getAreaRows(String area) {
        String[] seatRows = null;
        switch (area) {
        case "精華":
            seatRows = "IJ".split("");
            break;
        case "最佳":
            seatRows = "HIJK".split("");
            break;
        case "次佳":
            seatRows = "GHIJKL".split("");
            break;
        default: 
            seatRows = "ABCDEFGHIJKLM".split("");
            break;
        }
        return seatRows;
    }
    
}
