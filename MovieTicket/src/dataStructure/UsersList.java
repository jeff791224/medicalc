package dataStructure;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 這個class使用來存放Users 物件
 * @author Brian
 *
 */
public class UsersList extends ArrayList<Users>
{	
	/**
	 * 這個function 是用來把使用者加入list中
	 * @param id : 使用者ID
	 * @param name : 使用者名稱
	 * @param age : 使用者年齡
	 */
	public void add(int id, String name, int age) {
		super.add(new Users(id, name, age));
	}

	
}
