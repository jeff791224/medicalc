package dataStructure;

import java.util.ArrayList;

/**
 * 這個class 是用來存放多個SeatNum 物件
 */
public class SeatNumList extends ArrayList<SeatNum>
{	
	/**
	 * 這個function 會把各區座位數add到一個list中
	 * @param gray : 灰色區域座位數
	 * @param blue : 藍色區域座位數
	 * @param yellow : 黃色區域座位數
	 * @param red : 紅色區域座位數
	 * @throws Exception : 如果無法加入List 
	 */
	public void add(int gray, int blue, int yellow, int red) throws Exception {
		super.add(new SeatNum(gray,blue,yellow,red));
	}

}
