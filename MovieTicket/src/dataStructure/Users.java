package dataStructure;

/**
 * 這個class是用來存放使用者資料
 * @author Brian
 *
 */
public class Users
{
	private int _id;
	private String _name;
	private int _age;
	
	/**
	 * 這個constructor是用來設定使用者資料
	 * @param id : 使用者ID
	 * @param name : 使用者名字
	 * @param age : 使用者年齡
	 */
	public Users(int id, String name, int age)
	{
		this._id = id;
		this._name = name;
		this._age = age;
	}

	/**
	 * 這個function 是用來獲得使用者ID
	 * @return int : 使用者ID
	 */
	public int get_id()
	{
		return this._id;
	}

	/**
	 * 這個function是用來獲得使用者名稱
	 * @return String : 使用者名稱
	 */
	public String get_name()
	{
		return this._name;
	}
	
	/**
	 * 這個function 是用來獲得使用者年齡
	 * @return int : 使用者年齡
	 */
	public int get_age()
	{
		return this._age;
	}

}
