package dataStructure;

import java.time.LocalTime;

import dataAccess.*;

/**
 * 這個class是用來建立並測試DB Table
 * @author Brian
 *
 */
public class Test {
	public static void main(String[] args) {
		DBBuilder builder = new DBBuilder();
		try {
			builder.createDB();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
