/**
* This class implements the data structure for saving the regimen information
* for dosing recommendation and pharmacokinetics calculation.
* 
* @author  JeffShih
* @version 1.0
* @since   2017-06-23 
*/
public class Regimen {

	public double ld = 0;
	public double md = 0;
	public double tau = 0;
	public double tif = 0;
	
	public Regimen(){};
	
	public void SetRegimen	(double[] param) {
		/**
		 * this function set the parameter of regimen
		 * @param double[] for seting the parameter
		 * 
		*/
		this.ld = param[0];
		this.md = param[1];
		this.tau= param[2];
		this.tif= param[3];
	}

}
