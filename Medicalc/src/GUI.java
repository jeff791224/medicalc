import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

import net.miginfocom.swing.MigLayout;

public class GUI extends JFrame{

	
	
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		public GUIController guictlr;
		private JPanel contentPane;
		private JTextField txtPtID = new JTextField();
		private JTextField txtLDPrd1;
		private JTextField txtMDPrd1;
		private JTextField txtTauPrd1;
		private JTextField txtTIFPrd1;
		private JTextField txtLDPrd2;
		private JTextField txtMDPrd2;
		private JTextField txtTauPrd2;
		private JTextField txtTIFPrd2;
		private JTextField txtLDPrd3;
		private JTextField txtMDPrd3;
		private JTextField txtTauPrd3;
		private JTextField txtTIFPrd3;
		private JTextField txtT0Smp1;
		private JTextField txtT1Smp1;
		private JTextField txtC1Smp1;
		private JTextField txtT2Smp1;
		private JTextField txtC2Smp1;
		private JTextField txtT0Smp2;
		private JTextField txtT1Smp2;
		private JTextField txtC1Smp2;
		private JTextField txtT2Smp2;
		private JTextField txtC2Smp2;
		private JTextField txtT0Smp3;
		private JTextField txtT1Smp3;
		private JTextField txtC1Smp3;
		private JTextField txtT2Smp3;
		private JTextField txtC2Smp3;
		private JTextField txtPtName;
		private JTextField txtPtSex;
		private JTextField txtPtAge;
		private JTextField txtPtHt;
		private JTextField txtPtWt;
		private JTextField txtPtScr;
		private JTextField txtMDSmp1;
		private JTextField txtMDSmp2;
		private JTextField txtMDSmp3;
		private JTextField txtTauSmp1;
		private JTextField txtTauSmp2;
		private JTextField txtTauSmp3;
		private JTextField txtTIFSmp1;
		private JTextField txtTIFSmp2;
		private JTextField txtTIFSmp3;

		/**
		 * Launch the application.
		 */

		/**
		 * Create the frame.
		 */
		public GUI() {
			guictlr = new GUIController();
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, 500, 650);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			setTitle("Therapeutic Drug Monitoring Antibiotics");
			contentPane.setLayout(new FormLayout(new ColumnSpec[] {
					ColumnSpec.decode("right:80px"),
					FormSpecs.UNRELATED_GAP_COLSPEC,
					ColumnSpec.decode("left:140px:grow"),
					FormSpecs.UNRELATED_GAP_COLSPEC,
					ColumnSpec.decode("right:80px"),
					FormSpecs.UNRELATED_GAP_COLSPEC,
					ColumnSpec.decode("140px:grow"),},
				new RowSpec[] {
					RowSpec.decode("20px"),
					FormSpecs.RELATED_GAP_ROWSPEC,
					RowSpec.decode("20px"),
					FormSpecs.RELATED_GAP_ROWSPEC,
					RowSpec.decode("20px"),
					FormSpecs.RELATED_GAP_ROWSPEC,
					RowSpec.decode("20px"),
					FormSpecs.RELATED_GAP_ROWSPEC,
					RowSpec.decode("25px"),
					RowSpec.decode("550px"),}));
			
			JButton btnPtID = new JButton("ID");
			btnPtID.setHorizontalAlignment(SwingConstants.RIGHT);
			contentPane.add(btnPtID, "1, 1");
			btnPtID.setMinimumSize(new Dimension(40, 20));
			
			JSplitPane splitPane = new JSplitPane();
			contentPane.add(splitPane, "3, 1, fill, fill");
			
			txtPtID = new JTextField("");
			txtPtID.setMinimumSize(new Dimension(70, 21));
			splitPane.setLeftComponent(txtPtID);
			txtPtID.setMaximumSize(new Dimension(100, 2147483647));
			txtPtID.setColumns(10);
			
			JButton btnGo = new JButton("Go");
			splitPane.setRightComponent(btnGo);
			
			
			JLabel lblName = new JLabel("Name: ");
			contentPane.add(lblName, "5, 1, right, default");
			
			txtPtName = new JTextField(" ");
			txtPtName.setText("");
			txtPtName.setBackground(SystemColor.control);
			contentPane.add(txtPtName, "7, 1, fill, default");
			txtPtName.setColumns(10);
			
			JLabel lblSex = new JLabel("Sex: ");
			contentPane.add(lblSex, "1, 3, right, default");
			
			txtPtSex = new JTextField(" ");
			txtPtSex.setText("");
			txtPtSex.setColumns(10);
			txtPtSex.setBackground(SystemColor.menu);
			contentPane.add(txtPtSex, "3, 3, fill, default");
			
			JLabel lblAge = new JLabel("Age: ");
			contentPane.add(lblAge, "5, 3, right, default");
			
			txtPtAge = new JTextField(" ");
			txtPtAge.setText("");
			txtPtAge.setColumns(10);
			txtPtAge.setBackground(SystemColor.menu);
			contentPane.add(txtPtAge, "7, 3, fill, default");
			
			JLabel lblHt = new JLabel("Height (cm): ");
			contentPane.add(lblHt, "1, 5, right, default");
			
			txtPtHt = new JTextField(" ");
			txtPtHt.setText("");
			txtPtHt.setColumns(10);
			txtPtHt.setBackground(SystemColor.menu);
			contentPane.add(txtPtHt, "3, 5, fill, default");
			
			JLabel lblWt = new JLabel("Weight (kg): ");
			contentPane.add(lblWt, "5, 5, right, default");
			
			txtPtWt = new JTextField(" ");
			txtPtWt.setText("");
			txtPtWt.setColumns(10);
			txtPtWt.setBackground(SystemColor.menu);
			contentPane.add(txtPtWt, "7, 5, fill, default");
			
			JLabel lblScr = new JLabel("Scr (mg/dL): ");
			contentPane.add(lblScr, "1, 7, right, default");
			
			txtPtScr = new JTextField(" ");
			txtPtScr.setText("");
			txtPtScr.setColumns(10);
			txtPtScr.setBackground(SystemColor.menu);
			contentPane.add(txtPtScr, "3, 7, fill, default");
			
			JLabel lblIBW = new JLabel("IBW (kg): ");
			contentPane.add(lblIBW, "5, 7");
			
			JLabel lblPtIBW = new JLabel("IBW");
			contentPane.add(lblPtIBW, "7, 7");
			
			JLabel lblClcr = new JLabel("Clcr: ");
			contentPane.add(lblClcr, "1, 9");
			
			JLabel lblPtClcr = new JLabel("Clcr");
			contentPane.add(lblPtClcr, "3, 9");
			
			JLabel lbleGFR = new JLabel("eGFR:");
			contentPane.add(lbleGFR, "5, 9");
			
			JLabel lblPtEGFR = new JLabel("GFR");
			contentPane.add(lblPtEGFR, "7, 9");
			
			
			
			JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
			tabbedPane.setBounds(200,200,300,800);
			contentPane.add(tabbedPane, "1, 10, 7, 1, fill, fill");
			
			JPanel DosageRcmdPanel = new JPanel();
			tabbedPane.addTab("Dosage Recommendation", null, DosageRcmdPanel, null);
			DosageRcmdPanel.setLayout(new MigLayout("", "[200.00,grow,right][150.00,center][150,center]", "[center][20px][20px][20px][20px]"));
			
			JButton btncaldrugs = new JButton("Cal");
			btncaldrugs.setHorizontalAlignment(SwingConstants.LEFT);
			btncaldrugs.setVerticalAlignment(SwingConstants.TOP);
			btncaldrugs.setMinimumSize(new Dimension(40, 20));
			DosageRcmdPanel.add(btncaldrugs);
			
			
			JComboBox cbbDrugRcm = new JComboBox(guictlr.getDrugTradName());
			DosageRcmdPanel.add(cbbDrugRcm, "cell 0 0,growx");
			
				
			JLabel lblEmpiricalMethod = new JLabel("Empirical Dosing Method");
			DosageRcmdPanel.add(lblEmpiricalMethod, "cell 1 0,alignx center");
			
			JLabel lblOtherMethod = new JLabel("Other Dosing Method");
			DosageRcmdPanel.add(lblOtherMethod, "cell 2 0,alignx center");
			
			JLabel lblLDRcm = new JLabel("Loading Dose (mg)");
			DosageRcmdPanel.add(lblLDRcm, "cell 0 1");
			
			JLabel lblLDRcm0 = new JLabel("");
			DosageRcmdPanel.add(lblLDRcm0, "cell 1 1");
			
			JLabel lblLDRcm1 = new JLabel("");
			DosageRcmdPanel.add(lblLDRcm1, "cell 2 1");
			
			JLabel lblMDRcm = new JLabel("Maintenance Dose (mg)");
			DosageRcmdPanel.add(lblMDRcm, "cell 0 2");
			
			JLabel lblMDRcm0 = new JLabel("");
			DosageRcmdPanel.add(lblMDRcm0, "cell 1 2");
			
			JLabel lblMDRcm1 = new JLabel("");
			DosageRcmdPanel.add(lblMDRcm1, "cell 2 2");
			
			JLabel lblTauRcm = new JLabel("Dosing Interval (hr)");
			DosageRcmdPanel.add(lblTauRcm, "cell 0 3");
			
			JLabel lblTauRcm0 = new JLabel("");
			DosageRcmdPanel.add(lblTauRcm0, "cell 1 3");
			
			JLabel lblTauRcm1 = new JLabel("");
			DosageRcmdPanel.add(lblTauRcm1, "cell 2 3");
			
			JLabel lblTIFRcm = new JLabel("Infusion Time (hr)");
			DosageRcmdPanel.add(lblTIFRcm, "cell 0 4");
			
			JLabel lblTIFRcm0 = new JLabel("");
			DosageRcmdPanel.add(lblTIFRcm0, "cell 1 4");
			
			JLabel lblTIFRcm1 = new JLabel("");
			DosageRcmdPanel.add(lblTIFRcm1, "cell 2 4");
			
			
			cbbDrugRcm.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					lblOtherMethod.setText(guictlr.getOtherDosingMethodName(cbbDrugRcm.getSelectedItem().toString()));
					lblLDRcm0.setText(guictlr.getLDRcmd0());
					lblMDRcm0.setText(guictlr.getMDRcmd0());
					lblTauRcm0.setText(guictlr.getTauRcmd0());
					lblTIFRcm0.setText(guictlr.getTIFRcmd0());
					lblLDRcm1.setText(guictlr.getLDRcmd1());
					lblMDRcm1.setText(guictlr.getMDRcmd1());
					lblTauRcm1.setText(guictlr.getTauRcmd1());
					lblTIFRcm1.setText(guictlr.getTIFRcmd1());
				}
			});
			
			JPanel RegimenPrdPanel = new JPanel();
			tabbedPane.addTab("Regimen Prediction", null, RegimenPrdPanel, null);
			RegimenPrdPanel.setLayout(new MigLayout("", "[grow,right][100px,grow,center][100px,grow,center][100px,grow,center]", "[20px][20px][20px][20px][20px][20px][20px][20px][20px][20px][20px]"));
			
			

			JComboBox cbbDrugPrd = new JComboBox(guictlr.getDrugTradName());
			RegimenPrdPanel.add(cbbDrugPrd, "cell 0 0,growx");

			JButton btnPrd1 = new JButton("Regimen 1");
			RegimenPrdPanel.add(btnPrd1, "cell 1 0,alignx center");
			
			JButton btnPrd2 = new JButton("Regimen 2");
			RegimenPrdPanel.add(btnPrd2, "cell 2 0,alignx center");
			
			JButton btnPrd3 = new JButton("Regimen 3");
			RegimenPrdPanel.add(btnPrd3, "cell 3 0,alignx center");
			
			JLabel lblCldrugPrd = new JLabel("Cldrug (L/hr)");
			RegimenPrdPanel.add(lblCldrugPrd, "cell 0 1");
			
			JLabel lblCldrugPrd1 = new JLabel("");
			RegimenPrdPanel.add(lblCldrugPrd1, "cell 1 1");
			
			JLabel lblCldrugPrd2 = new JLabel("");
			RegimenPrdPanel.add(lblCldrugPrd2, "cell 2 1");
			
			JLabel lblCldrugPrd3 = new JLabel("");
			RegimenPrdPanel.add(lblCldrugPrd3, "cell 3 1");
			
			JLabel lblVdPrd = new JLabel("Vd (L)");
			RegimenPrdPanel.add(lblVdPrd, "cell 0 2");
			
			JLabel lblVdPrd1 = new JLabel("");
			RegimenPrdPanel.add(lblVdPrd1, "cell 1 2");
			
			JLabel lblVdPrd2 = new JLabel("");
			RegimenPrdPanel.add(lblVdPrd2, "cell 2 2");
			
			JLabel lblVdPrd3 = new JLabel("");
			RegimenPrdPanel.add(lblVdPrd3, "cell 3 2");
			
			JLabel lblKPrd = new JLabel("k (/hr)");
			RegimenPrdPanel.add(lblKPrd, "cell 0 3");
			
			JLabel lblKPrd1 = new JLabel("");
			RegimenPrdPanel.add(lblKPrd1, "cell 1 3");
			
			JLabel lblKPrd2 = new JLabel("");
			RegimenPrdPanel.add(lblKPrd2, "cell 2 3");
			
			JLabel lblKPrd3 = new JLabel("");
			RegimenPrdPanel.add(lblKPrd3, "cell 3 3");
			
			JLabel lblThalfPrd = new JLabel("t1/2 (hr)");
			RegimenPrdPanel.add(lblThalfPrd, "cell 0 4");
			
			JLabel lblThalfPrd1 = new JLabel("");
			RegimenPrdPanel.add(lblThalfPrd1, "cell 1 4");
			
			JLabel lblThalfPrd2 = new JLabel("");
			RegimenPrdPanel.add(lblThalfPrd2, "cell 2 4");
			
			JLabel lblThalfPrd3 = new JLabel("");
			RegimenPrdPanel.add(lblThalfPrd3, "cell 3 4");
			
			btnGo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					guictlr.getPaitentInfo(txtPtID.getText());
					guictlr.sethisdrug(cbbDrugPrd.getSelectedItem().toString());
					txtPtName.setText(guictlr.getPtName(txtPtID.getText()));
					txtPtSex.setText(guictlr.getPtSex(txtPtID.getText()));
					txtPtAge.setText(guictlr.getPtAge(txtPtID.getText()));
					txtPtHt.setText(guictlr.getPtHt(txtPtID.getText()));
					txtPtWt.setText(guictlr.getPtWt(txtPtID.getText()));
					txtPtScr.setText(guictlr.getPtScr(txtPtID.getText()));
					lblPtIBW.setText(guictlr.getPtIBW(txtPtID.getText()));
					lblPtClcr.setText(guictlr.getPtClcr(txtPtID.getText()));
					lblPtEGFR.setText(guictlr.getPtEGFR(txtPtID.getText()));
					lblLDRcm0.setText(guictlr.getLDRcmd0());
					lblMDRcm0.setText(guictlr.getMDRcmd0());
					lblTauRcm0.setText(guictlr.getTauRcmd0());
					lblTIFRcm0.setText(guictlr.getTIFRcmd0());
					lblLDRcm1.setText(guictlr.getLDRcmd1());
					lblMDRcm1.setText(guictlr.getMDRcmd1());
					lblTauRcm1.setText(guictlr.getTauRcmd1());
					lblTIFRcm1.setText(guictlr.getTIFRcmd1());
					lblCldrugPrd1.setText(guictlr.getCldrugPrdc());
					lblVdPrd1.setText(guictlr.getVdPrdc());
					lblKPrd1.setText(guictlr.getKPrdc());
					lblThalfPrd1.setText(guictlr.getThalfPrdc());
					lblCldrugPrd2.setText(guictlr.getCldrugPrdc());
					lblVdPrd2.setText(guictlr.getVdPrdc());
					lblKPrd2.setText(guictlr.getKPrdc());
					lblThalfPrd2.setText(guictlr.getThalfPrdc());
					lblCldrugPrd3.setText(guictlr.getCldrugPrdc());
					lblVdPrd3.setText(guictlr.getVdPrdc());
					lblKPrd3.setText(guictlr.getKPrdc());
					lblThalfPrd3.setText(guictlr.getThalfPrdc());
				}
			});
			
			btnPtID.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					guictlr.setPt(txtPtID.getText(), txtPtName.getText(), txtPtSex.getText(), txtPtAge.getText(), txtPtHt.getText(), txtPtWt.getText(), txtPtScr.getText());
					guictlr.getPaitentInfo(txtPtID.getText());
					lblPtIBW.setText(guictlr.getPtIBW(txtPtID.getText()));
					lblPtClcr.setText(guictlr.getPtClcr(txtPtID.getText()));
					lblPtEGFR.setText(guictlr.getPtEGFR(txtPtID.getText()));

				}
			});
			
			btncaldrugs.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					guictlr.sethisdrug(cbbDrugPrd.getSelectedItem().toString());
					lblLDRcm0.setText(guictlr.getLDRcmd0());
					lblMDRcm0.setText(guictlr.getMDRcmd0());
					lblTauRcm0.setText(guictlr.getTauRcmd0());
					lblTIFRcm0.setText(guictlr.getTIFRcmd0());
					lblLDRcm1.setText(guictlr.getLDRcmd1());
					lblMDRcm1.setText(guictlr.getMDRcmd1());
					lblTauRcm1.setText(guictlr.getTauRcmd1());
					lblTIFRcm1.setText(guictlr.getTIFRcmd1());
					lblCldrugPrd1.setText(guictlr.getCldrugPrdc());
					lblVdPrd1.setText(guictlr.getVdPrdc());
					lblKPrd1.setText(guictlr.getKPrdc());
					lblThalfPrd1.setText(guictlr.getThalfPrdc());
					lblCldrugPrd2.setText(guictlr.getCldrugPrdc());
					lblVdPrd2.setText(guictlr.getVdPrdc());
					lblKPrd2.setText(guictlr.getKPrdc());
					lblThalfPrd2.setText(guictlr.getThalfPrdc());
					lblCldrugPrd3.setText(guictlr.getCldrugPrdc());
					lblVdPrd3.setText(guictlr.getVdPrdc());
					lblKPrd3.setText(guictlr.getKPrdc());
					lblThalfPrd3.setText(guictlr.getThalfPrdc());
					
					
				}
			});
			

			
		
			JLabel lblLDPrd = new JLabel("Loading Dose (mg)");
			RegimenPrdPanel.add(lblLDPrd, "cell 0 5,alignx trailing");
			
			txtLDPrd1 = new JTextField();
			RegimenPrdPanel.add(txtLDPrd1, "cell 1 5,growx");
			txtLDPrd1.setColumns(10);
			
			txtLDPrd2 = new JTextField();
			txtLDPrd2.setColumns(10);
			RegimenPrdPanel.add(txtLDPrd2, "cell 2 5,growx");
			
			txtLDPrd3 = new JTextField();
			txtLDPrd3.setColumns(10);
			RegimenPrdPanel.add(txtLDPrd3, "cell 3 5,growx");
			
			JLabel lblMDPrd = new JLabel("Maintenance Dose (mg)");
			RegimenPrdPanel.add(lblMDPrd, "cell 0 6,alignx trailing");
			
			txtMDPrd1 = new JTextField();
			txtMDPrd1.setColumns(10);
			RegimenPrdPanel.add(txtMDPrd1, "cell 1 6,growx");
			
			txtMDPrd2 = new JTextField();
			txtMDPrd2.setColumns(10);
			RegimenPrdPanel.add(txtMDPrd2, "cell 2 6,growx");
			
			txtMDPrd3 = new JTextField();
			txtMDPrd3.setColumns(10);
			RegimenPrdPanel.add(txtMDPrd3, "cell 3 6,growx");
			
			JLabel lblTauPrd = new JLabel("Dosing Interval  (hr)");
			RegimenPrdPanel.add(lblTauPrd, "cell 0 7,alignx trailing");
			
			txtTauPrd1 = new JTextField();
			txtTauPrd1.setColumns(10);
			RegimenPrdPanel.add(txtTauPrd1, "cell 1 7,growx");
			
			txtTauPrd2 = new JTextField();
			txtTauPrd2.setColumns(10);
			RegimenPrdPanel.add(txtTauPrd2, "cell 2 7,growx");
			
			txtTauPrd3 = new JTextField();
			txtTauPrd3.setColumns(10);
			RegimenPrdPanel.add(txtTauPrd3, "cell 3 7,growx");
			
			JLabel lblTIFPrd = new JLabel("Infusion Time (hr)");
			RegimenPrdPanel.add(lblTIFPrd, "cell 0 8,alignx trailing");
			
			txtTIFPrd1 = new JTextField();
			txtTIFPrd1.setColumns(10);
			RegimenPrdPanel.add(txtTIFPrd1, "cell 1 8,growx");
			
			txtTIFPrd2 = new JTextField();
			txtTIFPrd2.setColumns(10);
			RegimenPrdPanel.add(txtTIFPrd2, "cell 2 8,growx");
			
			txtTIFPrd3 = new JTextField();
			txtTIFPrd3.setColumns(10);
			RegimenPrdPanel.add(txtTIFPrd3, "cell 3 8,growx");
			
			JLabel lblCpeakPrd = new JLabel("Cpeak (mg/L)");
			RegimenPrdPanel.add(lblCpeakPrd, "cell 0 9");
			
			JLabel lblCpeakPrd1 = new JLabel("");
			RegimenPrdPanel.add(lblCpeakPrd1, "cell 1 9");
			
			JLabel lblCpeakPrd2 = new JLabel("");
			RegimenPrdPanel.add(lblCpeakPrd2, "cell 2 9");
			
			JLabel lblCpeakPrd3 = new JLabel("");
			RegimenPrdPanel.add(lblCpeakPrd3, "cell 3 9");
			
			JLabel lblCtroughPrd = new JLabel("Ctrough (mg/L)");
			RegimenPrdPanel.add(lblCtroughPrd, "cell 0 10");
			
			JLabel lblCtroughPrd1 = new JLabel("");
			RegimenPrdPanel.add(lblCtroughPrd1, "cell 1 10");
			
			JLabel lblCtroughPrd2 = new JLabel("");
			RegimenPrdPanel.add(lblCtroughPrd2, "cell 2 10");
			
			JLabel lblCtroughPrd3 = new JLabel("");
			RegimenPrdPanel.add(lblCtroughPrd3, "cell 3 10");

			cbbDrugRcm.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					cbbDrugPrd.setSelectedItem(cbbDrugRcm.getSelectedItem().toString());
				}
			});

			cbbDrugPrd.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					lblCldrugPrd1.setText(guictlr.getCldrugPrdc());
					lblVdPrd1.setText(guictlr.getVdPrdc());
					lblKPrd1.setText(guictlr.getKPrdc());
					lblThalfPrd1.setText(guictlr.getThalfPrdc());
					lblCldrugPrd2.setText(guictlr.getCldrugPrdc());
					lblVdPrd2.setText(guictlr.getVdPrdc());
					lblKPrd2.setText(guictlr.getKPrdc());
					lblThalfPrd2.setText(guictlr.getThalfPrdc());
					lblCldrugPrd3.setText(guictlr.getCldrugPrdc());
					lblVdPrd3.setText(guictlr.getVdPrdc());
					lblKPrd3.setText(guictlr.getKPrdc());
					lblThalfPrd3.setText(guictlr.getThalfPrdc());
					txtLDPrd1.setText("");
					txtMDPrd1.setText("");
					txtTauPrd1.setText("");
					txtTIFPrd1.setText("");
					lblCpeakPrd1.setText("");
					lblCtroughPrd1.setText("");
					txtLDPrd2.setText("");
					txtMDPrd2.setText("");
					txtTauPrd2.setText("");
					txtTIFPrd2.setText("");
					lblCpeakPrd2.setText("");
					lblCtroughPrd2.setText("");
					txtLDPrd3.setText("");
					txtMDPrd3.setText("");
					txtTauPrd3.setText("");
					txtTIFPrd3.setText("");
					lblCpeakPrd3.setText("");
					lblCtroughPrd3.setText("");
				}
			});
			
			btnPrd1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					double[] param = {Double.parseDouble(txtLDPrd1.getText().toString()),Double.parseDouble(txtMDPrd1.getText().toString()),Double.parseDouble(txtTauPrd1.getText().toString()),Double.parseDouble(txtTIFPrd1.getText().toString())};
					Regimen regi = new Regimen();
					regi.SetRegimen(param);
					guictlr.patient.regi = regi;
					guictlr.sethisdrug(cbbDrugPrd.getSelectedItem().toString());
					lblCpeakPrd1.setText(guictlr.getCpeakPrd());
					lblCtroughPrd1.setText(guictlr.getCtroughPrd());
				}
			});
			
			btnPrd2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					double[] param = {Double.parseDouble(txtLDPrd2.getText().toString()),Double.parseDouble(txtMDPrd2.getText().toString()),Double.parseDouble(txtTauPrd2.getText().toString()),Double.parseDouble(txtTIFPrd2.getText().toString())};
					Regimen regi = new Regimen();
					regi.SetRegimen(param);
					guictlr.patient.regi = regi;
					guictlr.sethisdrug(cbbDrugPrd.getSelectedItem().toString());
					lblCpeakPrd2.setText(guictlr.getCpeakPrd());
					lblCtroughPrd2.setText(guictlr.getCtroughPrd());
				}
			});

			btnPrd3.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					double[] param = {Double.parseDouble(txtLDPrd3.getText().toString()),Double.parseDouble(txtMDPrd3.getText().toString()),Double.parseDouble(txtTauPrd3.getText().toString()),Double.parseDouble(txtTIFPrd3.getText().toString())};
					Regimen regi = new Regimen();
					regi.SetRegimen(param);
					guictlr.patient.regi = regi;
					guictlr.sethisdrug(cbbDrugPrd.getSelectedItem().toString());
					lblCpeakPrd3.setText(guictlr.getCpeakPrd());
					lblCtroughPrd3.setText(guictlr.getCtroughPrd());
				}
			});
			
			JPanel LevelSmpPanel = new JPanel();
			tabbedPane.addTab("Serum Level Sampling", null, LevelSmpPanel, null);
			LevelSmpPanel.setLayout(new MigLayout("", "[140.00,right][100px,grow,center][100px,grow,center][100px,grow,center]", "[][][][][][][][][][20px][20px][20px][20px][20px][20px]"));
			
				
			JComboBox cbbDrugSmp = new JComboBox(guictlr.getDrugTradName());
			LevelSmpPanel.add(cbbDrugSmp, "cell 0 0,growx");
			
			JButton btnSmp1 = new JButton("Dose 1");
			LevelSmpPanel.add(btnSmp1, "cell 1 0");
			
			JButton btnSmp2 = new JButton("Dose 2");
			LevelSmpPanel.add(btnSmp2, "cell 2 0");
			
			JButton btnSmp3 = new JButton("Dose 3");
			LevelSmpPanel.add(btnSmp3, "cell 3 0");
			
			JLabel lblMDSmp = new JLabel("Dose (mg)");
			LevelSmpPanel.add(lblMDSmp, "cell 0 1,alignx trailing");
			
			txtMDSmp1 = new JTextField();
			LevelSmpPanel.add(txtMDSmp1, "cell 1 1,growx");
			txtMDSmp1.setColumns(10);
			
			txtMDSmp2 = new JTextField();
			LevelSmpPanel.add(txtMDSmp2, "cell 2 1,growx");
			txtMDSmp2.setColumns(10);
			
			txtMDSmp3 = new JTextField();
			LevelSmpPanel.add(txtMDSmp3, "cell 3 1,growx");
			txtMDSmp3.setColumns(10);
			
			JLabel lblTauSmp = new JLabel("Dosing Interval (hr)");
			LevelSmpPanel.add(lblTauSmp, "cell 0 2,alignx trailing");
			
			txtTauSmp1 = new JTextField();
			LevelSmpPanel.add(txtTauSmp1, "cell 1 2,growx");
			txtTauSmp1.setColumns(10);
			
			txtTauSmp2 = new JTextField();
			LevelSmpPanel.add(txtTauSmp2, "cell 2 2,growx");
			txtTauSmp2.setColumns(10);
			
			txtTauSmp3 = new JTextField();
			LevelSmpPanel.add(txtTauSmp3, "cell 3 2,growx");
			txtTauSmp3.setColumns(10);
			
			JLabel lblTIFSmp = new JLabel("Infusion Time (hr)");
			LevelSmpPanel.add(lblTIFSmp, "cell 0 3,alignx trailing");
			
			txtTIFSmp1 = new JTextField();
			LevelSmpPanel.add(txtTIFSmp1, "cell 1 3,growx");
			txtTIFSmp1.setColumns(10);
			
			txtTIFSmp2 = new JTextField();
			LevelSmpPanel.add(txtTIFSmp2, "cell 2 3,growx");
			txtTIFSmp2.setColumns(10);
			
			txtTIFSmp3 = new JTextField();
			LevelSmpPanel.add(txtTIFSmp3, "cell 3 3,growx");
			txtTIFSmp3.setColumns(10);
			
			JLabel lblT0Smp = new JLabel("Dosing Time (hr:min)");
			LevelSmpPanel.add(lblT0Smp, "cell 0 4,alignx trailing");
			
			txtT0Smp1 = new JTextField();
			LevelSmpPanel.add(txtT0Smp1, "cell 1 4,growx");
			txtT0Smp1.setColumns(10);
			
			txtT0Smp2 = new JTextField();
			LevelSmpPanel.add(txtT0Smp2, "cell 2 4,growx");
			txtT0Smp2.setColumns(10);
			
			txtT0Smp3 = new JTextField();
			LevelSmpPanel.add(txtT0Smp3, "cell 3 4,growx");
			txtT0Smp3.setColumns(10);
			
			JLabel lblT1Smp = new JLabel("Sample 1 Time (hr:min)");
			LevelSmpPanel.add(lblT1Smp, "cell 0 5");
			
			txtT1Smp1 = new JTextField();
			LevelSmpPanel.add(txtT1Smp1, "cell 1 5,growx");
			txtT1Smp1.setColumns(10);
			
			txtT1Smp2 = new JTextField();
			LevelSmpPanel.add(txtT1Smp2, "cell 2 5,growx");
			txtT1Smp2.setColumns(10);
			
			txtT1Smp3 = new JTextField();
			LevelSmpPanel.add(txtT1Smp3, "cell 3 5,growx");
			txtT1Smp3.setColumns(10);
			
			JLabel lblC1Smp = new JLabel("Sample 1 Level (mg/L)");
			LevelSmpPanel.add(lblC1Smp, "cell 0 6");
			
			txtC1Smp1 = new JTextField();
			LevelSmpPanel.add(txtC1Smp1, "cell 1 6,growx");
			txtC1Smp1.setColumns(10);
			
			txtC1Smp2 = new JTextField();
			LevelSmpPanel.add(txtC1Smp2, "cell 2 6,growx");
			txtC1Smp2.setColumns(10);
			
			txtC1Smp3 = new JTextField();
			LevelSmpPanel.add(txtC1Smp3, "cell 3 6,growx");
			txtC1Smp3.setColumns(10);
			
			JLabel lblT2Smp = new JLabel("Sample 2 Time (hr:min)");
			LevelSmpPanel.add(lblT2Smp, "cell 0 7,alignx trailing");
			
			txtT2Smp1 = new JTextField();
			LevelSmpPanel.add(txtT2Smp1, "cell 1 7,growx");
			txtT2Smp1.setColumns(10);
			
			txtT2Smp2 = new JTextField();
			LevelSmpPanel.add(txtT2Smp2, "cell 2 7,growx");
			txtT2Smp2.setColumns(10);
			
			txtT2Smp3 = new JTextField();
			LevelSmpPanel.add(txtT2Smp3, "cell 3 7,growx");
			txtT2Smp3.setColumns(10);
			
			JLabel lblC2Smp = new JLabel("Sample 2 Level (mg/L)");
			LevelSmpPanel.add(lblC2Smp, "cell 0 8,alignx trailing");
			
			txtC2Smp1 = new JTextField();
			LevelSmpPanel.add(txtC2Smp1, "cell 1 8,growx");
			txtC2Smp1.setColumns(10);
			
			txtC2Smp2 = new JTextField();
			LevelSmpPanel.add(txtC2Smp2, "cell 2 8,growx");
			txtC2Smp2.setColumns(10);
			
			txtC2Smp3 = new JTextField();
			LevelSmpPanel.add(txtC2Smp3, "cell 3 8,growx");
			txtC2Smp3.setColumns(10);
			
			JLabel lblKSmp = new JLabel("k (/hr)");
			LevelSmpPanel.add(lblKSmp, "cell 0 9");
			
			JLabel lblKSmp1 = new JLabel("");
			LevelSmpPanel.add(lblKSmp1, "cell 1 9");
			
			JLabel lblKSmp2 = new JLabel("");
			LevelSmpPanel.add(lblKSmp2, "cell 2 9");
			
			JLabel lblKSmp3 = new JLabel("");
			LevelSmpPanel.add(lblKSmp3, "cell 3 9");
			
			JLabel lblThalfSmp = new JLabel("t1/2 (hr)");
			LevelSmpPanel.add(lblThalfSmp, "cell 0 10");
			
			JLabel lblThalfSmp1 = new JLabel("");
			LevelSmpPanel.add(lblThalfSmp1, "cell 1 10");
			
			JLabel lblThalfSmp2 = new JLabel("");
			LevelSmpPanel.add(lblThalfSmp2, "cell 2 10");
			
			JLabel lblThalfSmp3 = new JLabel("");
			LevelSmpPanel.add(lblThalfSmp3, "cell 3 10");
			
			JLabel lblVdSmp = new JLabel("Vd (L)");
			LevelSmpPanel.add(lblVdSmp, "cell 0 11");
			
			JLabel lblVdSmp1 = new JLabel("");
			LevelSmpPanel.add(lblVdSmp1, "cell 1 11");
			
			JLabel lblVdSmp2 = new JLabel("");
			LevelSmpPanel.add(lblVdSmp2, "cell 2 11");
			
			JLabel lblVdSmp3 = new JLabel("");
			LevelSmpPanel.add(lblVdSmp3, "cell 3 11");
			
			JLabel lblCldrugSmp = new JLabel("Cldrug (L/hr)");
			LevelSmpPanel.add(lblCldrugSmp, "cell 0 12");
			
			JLabel lblCldrugSmp1 = new JLabel("");
			LevelSmpPanel.add(lblCldrugSmp1, "cell 1 12");
			
			JLabel lblCldrugSmp2 = new JLabel("");
			LevelSmpPanel.add(lblCldrugSmp2, "cell 2 12");
			
			JLabel lblCldrugSmp3 = new JLabel("");
			LevelSmpPanel.add(lblCldrugSmp3, "cell 3 12");
			
			JLabel lblCpeakSmp = new JLabel("Cpeak (unit)");
			LevelSmpPanel.add(lblCpeakSmp, "cell 0 13");
			
			JLabel lblCpeakSmp1 = new JLabel("");
			LevelSmpPanel.add(lblCpeakSmp1, "cell 1 13");
			
			JLabel lblCpeakSmp2 = new JLabel("");
			LevelSmpPanel.add(lblCpeakSmp2, "cell 2 13");
			
			JLabel lblCpeakSmp3 = new JLabel("");
			LevelSmpPanel.add(lblCpeakSmp3, "cell 3 13");
			
			JLabel lblCtroughSmp = new JLabel("Ctrough (unit)");
			LevelSmpPanel.add(lblCtroughSmp, "cell 0 14");
			
			JLabel lblCtroughSmp1 = new JLabel("");
			LevelSmpPanel.add(lblCtroughSmp1, "cell 1 14");
			
			JLabel lblCtroughSmp2 = new JLabel("");
			LevelSmpPanel.add(lblCtroughSmp2, "cell 2 14");
			
			JLabel lblCtroughSmp3 = new JLabel("");
			LevelSmpPanel.add(lblCtroughSmp3, "cell 3 14");

				
			cbbDrugRcm.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					cbbDrugSmp.setSelectedItem(cbbDrugRcm.getSelectedItem().toString());
				}
			});
			
			cbbDrugPrd.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					cbbDrugSmp.setSelectedItem(cbbDrugPrd.getSelectedItem().toString());
				}
			});
			
			
			cbbDrugSmp.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					txtT0Smp1.setText("");
					txtT1Smp1.setText("");
					txtC1Smp1.setText("");
					txtT2Smp1.setText("");
					txtC2Smp1.setText("");
					lblKSmp1.setText("");
					lblThalfSmp1.setText("");
					lblVdSmp1.setText("");
					lblCldrugSmp1.setText("");
					lblCpeakSmp1.setText("");
					lblCtroughSmp1.setText("");
					txtT0Smp2.setText("");
					txtT1Smp2.setText("");
					txtC1Smp2.setText("");
					txtT2Smp2.setText("");
					txtC2Smp2.setText("");
					lblKSmp2.setText("");
					lblThalfSmp2.setText("");
					lblVdSmp2.setText("");
					lblCldrugSmp2.setText("");
					lblCpeakSmp2.setText("");
					lblCtroughSmp2.setText("");
					txtT0Smp3.setText("");
					txtT1Smp3.setText("");
					txtC1Smp3.setText("");
					txtT2Smp3.setText("");
					txtC2Smp3.setText("");
					lblKSmp3.setText("");
					lblThalfSmp3.setText("");
					lblVdSmp3.setText("");
					lblCldrugSmp3.setText("");
					lblCpeakSmp3.setText("");
					lblCtroughSmp3.setText("");
				}
			});
			
			btnSmp1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					double t0 = Double.parseDouble(txtT0Smp1.getText().split(":")[0])+Double.parseDouble(txtT0Smp1.getText().split(":")[1])/60;
					double t1 = Double.parseDouble(txtT1Smp1.getText().split(":")[0])+Double.parseDouble(txtT1Smp1.getText().split(":")[1])/60;
					double t2 = Double.parseDouble(txtT2Smp1.getText().split(":")[0])+Double.parseDouble(txtT2Smp1.getText().split(":")[1])/60;
					

					if (Double.parseDouble(txtTauSmp1.getText())>=24)
						t2 +=24;
					else
						if(t0>t2)
							t2+=24;
					
					double[] param = { Double.parseDouble(txtC1Smp1.getText()),
									   Double.parseDouble(txtC2Smp1.getText()),
									   t1-t0,t2-t0,
									   Double.parseDouble(txtMDSmp1.getText()),
									   Double.parseDouble(txtTauSmp1.getText()),
									   Double.parseDouble(txtTIFSmp1.getText())};
					guictlr.smp.setSmp(param);
					lblKSmp1.setText(guictlr.getKSmp());
					lblThalfSmp1.setText(guictlr.getThalfSmp());
					lblVdSmp1.setText(guictlr.getVdSmp());
					lblCldrugSmp1.setText(guictlr.getCldrugSmp());
					lblCpeakSmp1.setText(guictlr.getCpeakSmp());
					lblCtroughSmp1.setText(guictlr.getCtroughSmp());
				}
			});

			btnSmp2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					double t0 = Double.parseDouble(txtT0Smp2.getText().split(":")[0])+Double.parseDouble(txtT0Smp2.getText().split(":")[1])/60;
					double t1 = Double.parseDouble(txtT1Smp2.getText().split(":")[0])+Double.parseDouble(txtT1Smp2.getText().split(":")[1])/60;
					double t2 = Double.parseDouble(txtT2Smp2.getText().split(":")[0])+Double.parseDouble(txtT2Smp2.getText().split(":")[1])/60;

					if (Double.parseDouble(txtTauSmp2.getText())>=24)
						t2 +=24;
					else
						if(t0>t2)
							t2+=24;
					
					double[] param = { Double.parseDouble(txtC1Smp2.getText()),
							   Double.parseDouble(txtC2Smp2.getText()),
							   t1-t0,t2-t0,
							   Double.parseDouble(txtMDSmp2.getText()),
							   Double.parseDouble(txtTauSmp2.getText()),
							   Double.parseDouble(txtTIFSmp2.getText())};
					guictlr.smp.setSmp(param);
					lblKSmp2.setText(guictlr.getKSmp());
					lblThalfSmp2.setText(guictlr.getThalfSmp());
					lblVdSmp2.setText(guictlr.getVdSmp());
					lblCldrugSmp2.setText(guictlr.getCldrugSmp());
					lblCpeakSmp2.setText(guictlr.getCpeakSmp());
					lblCtroughSmp2.setText(guictlr.getCtroughSmp());
				}
			});
			
			btnSmp3.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					double t0 = Double.parseDouble(txtT0Smp3.getText().split(":")[0])+Double.parseDouble(txtT0Smp3.getText().split(":")[1])/60;
					double t1 = Double.parseDouble(txtT1Smp3.getText().split(":")[0])+Double.parseDouble(txtT1Smp3.getText().split(":")[1])/60;
					double t2 = Double.parseDouble(txtT2Smp3.getText().split(":")[0])+Double.parseDouble(txtT2Smp3.getText().split(":")[1])/60;
					
					if (Double.parseDouble(txtTauSmp3.getText())>=24)
						t2 +=24;
					else
						if(t0>t2)
							t2+=24;
					
					double[] param = { Double.parseDouble(txtC1Smp3.getText()),
							   Double.parseDouble(txtC2Smp3.getText()),
							   t1-t0,t2-t0,
							   Double.parseDouble(txtMDSmp3.getText()),
							   Double.parseDouble(txtTauSmp3.getText()),
							   Double.parseDouble(txtTIFSmp3.getText())};
					guictlr.smp.setSmp(param);
					lblKSmp3.setText(guictlr.getKSmp());
					lblThalfSmp3.setText(guictlr.getThalfSmp());
					lblVdSmp3.setText(guictlr.getVdSmp());
					lblCldrugSmp3.setText(guictlr.getCldrugSmp());
					lblCpeakSmp3.setText(guictlr.getCpeakSmp());
					lblCtroughSmp3.setText(guictlr.getCtroughSmp());
				}
			});
			
	
	
		}
		
		public static void main(String[] args) {
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						GUI frame = new GUI();
						frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}

}
