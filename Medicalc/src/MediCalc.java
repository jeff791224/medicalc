/**
* This MediCalc program implements a calculator
* for pharmacy calculation.
* 
* @author  JeffShih
* @version 1.0
* @since   2017-06-23 
*/





import java.awt.EventQueue;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;




/**
* This MediCalc program implements a calculator
* for pharmacy calculation.
* 
* @This is the main class for testing and program execute entry point.
*/
public class MediCalc {

	public MediCalc() {
		// TODO Auto-generated constructor stub
	}

	
	
	
	private static void testsql(){
		try{
		//Class.forName ("oracle.jdbc.OracleDriver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/Medicalc","jeff","jeff");
		Statement mystm = conn.createStatement();
		ResultSet rs = mystm.executeQuery("Select * from MedicalRecord");
		
		while(rs.next()){
		if(rs.getString("Obesity").equals("1")){
			System.out.println(rs.getString(2) + " is obese");
		}
		}
		
		}catch(Exception exc){
			exc.printStackTrace();
			}
		
		
	}
	


	
	private static void testMRquery(){
		
		//parameter: id,gender,ht,wt,age,scr
		int ptid=0;
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter the Patient id:");
		ptid = Integer.parseInt(scan.next());
		MedicalRecord curMr = new MedicalRecord();
		curMr.queryPatientRecord(ptid);
		System.out.println(curMr.getClcr());
		System.out.println(curMr.getEGFR());
		System.out.println(curMr.getIBW());
		System.out.println("This guy is "+(curMr.getOB()?"obese":"not obese"));
	}
	
	private static void testPatients(){
		int ptid = 33618;
		
		double[] regiparam = {1000,500,8,1};
		
		Patient David = new Patient();
		
		David.regi.SetRegimen(regiparam);
		
		David.mr.queryPatientRecord(ptid);
		David.rcm = David.rmds.setDrug(Drugs.Type.Vancomycin,David.mr);
		David.pk = David.drug.setDrug(Drugs.Type.Vancomycin, David.mr, David.rcm,David.regi);
		
		System.out.println(David.rcm.getld1info());
		System.out.println("vd outside pk = ");
		System.out.println(David.pk.getVd());
		System.out.println(David.pk.getcldrug());
		System.out.println(David.pk.getK());
		System.out.println(David.pk.getThalf());
		System.out.println(David.pk.getCpeak());
		System.out.println(David.pk.getCtrough());
		System.out.println(David.mr.getName());
	}
	
	private static void testctlr(){
		
		int ptid = 33618;
		
		double[] regiparam = {1000,500,8,1};
		
		Patient David = new Patient();
		
		David.regi.SetRegimen(regiparam);
		
		David.mr.queryPatientRecord(ptid);
		David.rcm = David.rmds.setDrug(Drugs.Type.Vancomycin,David.mr);
		David.pk = David.drug.setDrug(Drugs.Type.Vancomycin, David.mr, David.rcm,David.regi);
		
		
		GUIController guictlr = new GUIController();
		
		
		
	}
	
 
	

	public static void main(String[] args) throws NumberFormatException, SQLException{
		
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
}
