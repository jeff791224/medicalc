/**
* This is the abstract class implements pharmakinetic method for concentration recommendation
* for pharmacy calculation.
* 
* @author  JeffShih
* @version 1.0
* @since   2017-06-23 
*/

public abstract class PK {

	/**
	 * parameter: 
	 * 0: cldrug
	 * 1: vd
	 * 2: k
	 * 3: tif
	 * 4: tau
	 * 5: md
	 */
	
	private double cldrug;
	private double vd;
	private double k;
	private double thalf;// = Math.log(2) / k;
	private double tpeak;//tif
	private double ttrough;//tau
	private double md;
	private double cpeak;//(md / vd) / (1 - Math.Exp(-k * tau)) * Math.Exp(-k * tpeak);
	private double ctrough;//(md / vd) / (1 - Math.Exp(-k * tau)) * Math.Exp(-k * ttrough);
	
	
	public PK(MedicalRecord mr,Recommendation rmt,Regimen regi){
	}
	
	public abstract double getcldrug();
	public abstract double getVd();
	public abstract double getK();
	public abstract double getThalf();
	public abstract double getCpeak();
	public abstract double getCtrough();
	public abstract void setcldrug(MedicalRecord mr,Recommendation rmt);
	public abstract void setvd(MedicalRecord mr,Recommendation rmt);
	public abstract void setk(MedicalRecord mr,Recommendation rmt);
	

}
