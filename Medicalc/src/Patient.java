/**
* This class implements the main clas for data handling, 
* which contains every data structure class the calculation needs.
* 
* @author  JeffShih
* @version 1.0
* @since   2017-06-23 
*/

public class Patient {

	/**
	 * @param mr for MedicalRecord
	 * @param drug for drug factory
	 * @param PK for PharmacoKinetic calculation
	 * @param Regimen is a data structor for regimen data transfer
	 * @param Recommendations is the Recommendation factory
	 * @param Recommendation is the calculation class for emprical and specific method.  
	 */
	
	public MedicalRecord mr = new MedicalRecord();
	public Drugs drug = new Drugs();
	public PK pk;
	public Regimen regi = new Regimen();
	public Recommendations rmds = new Recommendations();
	public Recommendation rcm;

	
	public Patient() {
		/**
		 * This is the default constructor
		 */
		}

}
