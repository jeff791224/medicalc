/**
 * GUIController class contain methods gathering information for GUI 
 * 
 * @author B01801036
 * @version 4.0
 * @since 2017-06-15
 */

public class GUIController {

	/**
	 * patient to evaluate 
	 */
	public Patient patient = new Patient();
	
	/**
	 * medical record of the patient 
	 */
	public MedicalRecord mr;
	
	/**
	 * all drugs to monitor in the patient 
	 */
	public Drugs ptdrug;
	
	/**
	 * all dosage recommendation for the patient 
	 */
	public Recommendations rmds = new Recommendations();
	
	/**
	 * specific dosage recommendation for the patient
	 */
	public Recommendation rcm;
	
	/**
	 *  specific drug to monitor in the patient
	 */
	public Drugs.Type thisdrugname;
	
	/**
	 *  pharmacokinetic parameter of the patient
	 */
	public PK thispk;
	
	/**
	 *  specific regimen
	 */
	public Regimen regi;
	
	/**
	 *  serum level sampling of the patient 
	 */
	public Sampling smp = new Sampling();
	
	/**
	 * setSmp method can set the regimen and sampling result  
	 * 
	 * @param param[]
	 *            the regimen and sampling result  
	 */
	public void setSmp(double param[]){
		smp.setSmp(param);
	}
	
	/**
	 * getPatientInfo method can set the patient with specified patient ID  
	 * 
	 * @param ptID
	 *            patient ID  
	 */
	public void getPaitentInfo(String ptID){
		patient.mr.queryPatientRecord(Integer.parseInt(ptID));
		this.mr = patient.mr;
		this.ptdrug = patient.drug;
		this.rmds = patient.rmds;
	}
	
	/**
	 * getPtName method can return the name of the patient  
	 * 
	 * @param ptID
	 *            patient ID
	 * @return name of the patient   
	 */
	public String getPtName(String ptID) {
		return mr.getName();
	}
	
	/**
	 * getPtSex method can return the gender of the patient  
	 * 
	 * @param ptID
	 *            patient ID
	 * @return gender of the patient   
	 */
	public String getPtSex(String ptID) {
		return mr.getSex();
	}
	
	/**
	 * getPtAge method can return the age of the patient  
	 * 
	 * @param ptID
	 *            patient ID
	 * @return age of the patient   
	 */
	public String getPtAge(String ptID) {
		return Integer.toString(mr.getAge());
	}

	/**
	 * getPtHt method can return the height of the patient  
	 * 
	 * @param ptID
	 *            patient ID
	 * @return height of the patient   
	 */
	public String getPtHt(String ptID) {
		return Double.toString(mr.getHt());
	}

	/**
	 * getPtWt method can return the weight of the patient  
	 * 
	 * @param ptID
	 *            patient ID
	 * @return weight of the patient   
	 */
	public String getPtWt(String ptID) {
		return Double.toString(mr.getWt());
	}
	
	/**
	 * getPtScr method can return the level serum creatinine of the patient  
	 * 
	 * @param ptID
	 *            patient ID
	 * @return level of serum creatinine of the patient   
	 */
	public  String getPtScr(String ptID) {
		return Double.toString(mr.getScr());
	}
	
	/**
	 * getPtIBW method can return the ideal body weight of the patient  
	 * 
	 * @param ptID
	 *            patient ID
	 * @return ideal body weight of the patient   
	 */
	public  String getPtIBW(String ptID) {
		double ibw = Math.round(mr.getIBW() * 100.0) / 100.0;
		return Double.toString(ibw);
	}
	
	/**
	 * getPtClcr method can return the creatinine clearance of the patient  
	 * 
	 * @param ptID
	 *            patient ID
	 * @return cratinine clearance of the patient   
	 */
	public  String getPtClcr(String ptID) {
		double clcr = Math.round(mr.getClcr() * 100.0) / 100.0;
		return Double.toString(clcr);
	}
	
	/**
	 * getPtEGFR method can return the estimated glomerular filtration rate of the patient  
	 * 
	 * @param ptID
	 *            patient ID
	 * @return estimated glomerular filtration rate of the patient   
	 */
	public  String getPtEGFR(String ptID) {
		double egfr = Math.round(mr.getEGFR() * 100.0) / 100.0;
		return Double.toString(egfr);
	}
	
	/**
	 * setPt method can set the medical record of the patient  
	 * 
	 * @param ptID
	 *            patient ID
	 * @param ptName
	 *            patient name
	 * @param ptSex
	 *            patient gender 
	 * @param ptAge
	 *            patient age
	 * @param ptHt
	 *            patient height
	 * @param ptWt
	 *            patient weight
	 * @param ptScr
	 *            patient serum creatinine level    
	 */
	public  void setPt(String ptID, String ptName, String ptSex, String ptAge, String ptHt, String ptWt, String ptScr) {
		String[] param = {ptID,ptSex,ptHt,ptWt,ptAge,ptScr,ptName};
		Patient newp = new Patient();
		newp.mr.setPatientRecord(param);
	}
	
	/**
	 * getDrugTradName method can return the name of all the drugs can be evaluated  
	 * 
	 * @return the name of all the drugs can be evaluated   
	 */
	public String[] getDrugTradName() {
		return Drugs.TradeName;
	}
	
	/**
	 * getDrugGenericName method can return the generic name of a specific trade name  
	 * 
	 * @param drugTradeName
	 *            trade name of a drug
	 * @return generic name of a specific trade name   
	 */
	private  String getDrugGenericName(String drugTradeName) {
		return drugTradeName.split(" ")[0];
	}
	
	/**
	 * getOtherDosingMethodName method can return the name of a dosing method of a drug other than the empirical method  
	 * 
	 * @param drugName
	 *            name of a drug
	 * @return name of another dosing method other than empirical method   
	 */
	public  String getOtherDosingMethodName(String drugName) {
		String drug = getDrugGenericName(drugName);
		switch (drug) {
		case "vancomycin":
			return Drugs.OtherDosingMethod[0];
		case "gentamicin":
			return Drugs.OtherDosingMethod[1];
		case "amikacin":
			return Drugs.OtherDosingMethod[2];
		default:
			return Drugs.OtherDosingMethod[0];
		}
	}
	
	/**
	 * sethisdrug method can set the drug to be evaluated  
	 * 
	 * @param drugName
	 *            drug name
	 */
	public void sethisdrug(String drugName){
		 switch(drugName.split(" ")[0]){
		 case "vancomycin":
			 this.thisdrugname = Drugs.Type.Vancomycin;
			 break;
		 case "gentamicin":
			 this.thisdrugname = Drugs.Type.Gentamicin;
			 break;
		 case "amikacin":
			 this.thisdrugname = Drugs.Type.Amikacin;
			 break;
		 }
		 this.rcm = this.rmds.setDrug(this.thisdrugname, mr);
		 this.thispk = patient.drug.setDrug(this.thisdrugname, mr, this.rcm, patient.regi);
	}
	
	/**
	 * getLDRcmd0 method can return the empirical recommended loading dose  
	 * 
	 * @return empirical recommended loading dose   
	 */
	public String getLDRcmd0() {
		return this.rcm.getld0info();
	}

	/**
	 * getMDRcmd0 method can return the empirical recommended maintenance dose  
	 * 
	 * @return empirical recommended maintenance dose   
	 */
	public  String getMDRcmd0() {
		return this.rcm.getmd0info();
	}

	/**
	 * getTauRcmd0 method can return the empirical recommended dosing interval  
	 * 
	 * @return empirical recommended dosing interval   
	 */
	public  String getTauRcmd0() {
		return this.rcm.gettau0info();
	}
	
	/**
	 * getTIFRcmd0 method can return the empirical recommended infusion time  
	 * 
	 * @return empirical recommended infusion time
	 */
	public  String getTIFRcmd0() {
		return this.rcm.gettif0info();
	}

	/**
	 * getLDRcmd1 method can return the other recommended loading dose  
	 * 
	 * @return other recommended loading dose   
	 */
	public  String getLDRcmd1() {
		return this.rcm.getld1info();
	}

	/**
	 * getMDRcmd1 method can return the other recommended maintenance dose  
	 * 
	 * @return other recommended maintenance dose   
	 */
	public  String getMDRcmd1() {
		return this.rcm.getmd1info();
	}

	/**
	 * getTauRcmd1 method can return the other recommended dosing interval  
	 * 
	 * @return other recommended dosing interval
	 */
	public  String getTauRcmd1() {
		return this.rcm.gettau1info();
	}
	
	/**
	 * getTIFRcmd1 method can return the other recommended infusion time  
	 * 
	 * @return other recommended infusion time
	 */
	public  String getTIFRcmd1() {
		return this.rcm.gettif1info();
	}

	/**
	 * getCldrugPrd method can return the predicted clearance of drug  
	 * 
	 * @return predicted clearance of drug
	 */
	public  String getCldrugPrdc() {
		return Double.toString(this.thispk.getcldrug());
	}
	
	/**
	 * getVdPrd method can return the predicted volume of distribution  
	 * 
	 * @return predicted volume of distribution
	 */
	public  String getVdPrdc() {
		return Double.toString(this.thispk.getVd());
	}
	
	/**
	 * getKPrd method can return the predicted elimination constant  
	 * 
	 * @return predicted elimination constant
	 */
	public  String getKPrdc() {
		return Double.toString(this.thispk.getK());
	}
	
	/**
	 * getThalfPrd method can return the predicted half life   
	 * 
	 * @return predicted half life
	 */
	public  String getThalfPrdc() {
		return Double.toString(this.thispk.getThalf());
	}
	
	/**
	 * getCpeakPrd method can return the predicted peak level in the patient  
	 * 
	 * @return predicted peak level in the patient
	 */
	public  String getCpeakPrd(){
		return Double.toString(this.thispk.getCpeak());
	}
	
	/**
	 * getCtroughPrd method can return the predicted trough level in the patient  
	 * 
	 * @return predicted trough level in the patient
	 */
	public  String getCtroughPrd(){
		return Double.toString(this.thispk.getCtrough());
	}
	
	/**
	 * getKSmp method can return the elimination constant calculated by the sampling result  
	 * 
	 * @return elimination constant calculated by the sampling result
	 */
	public  String getKSmp(){
		return this.smp.getKSmpInfo();
	}
	
	/**
	 * getThalfSmp method can return the half life calculated by the sampling result  
	 * 
	 * @return half life calculated by the sampling result
	 */
	public  String getThalfSmp() {
		return this.smp.getThalfSmpInfo();
	}
	
	/**
	 * getVdSmp method can return the volume of distribution calculated by the sampling result  
	 * 
	 * @return volume of distribution calculated by the sampling result
	 */
	public  String getVdSmp() {
		return this.smp.getVdSmpInto();
	}
	
	/**
	 * getCldrugSmp method can return the clearance of drug calculated by the sampling result  
	 * 
	 * @return clearance of drug calculated by the sampling result
	 */
	public  String getCldrugSmp() {
		return this.smp.getCldrugSmpInfo();
	}
	
	/**
	 * getCpeakSmp method can return the peak level calculated by the sampling result  
	 * 
	 * @return peak level calculated by the sampling result
	 */
	public  String getCpeakSmp() {
		return this.smp.getCpeakSmpInfo();
	}
	
	/**
	 * getCtroughSmp method can return the trough level calculated by the sampling result  
	 * 
	 * @return trough level calculated by the sampling result
	 */
	public  String getCtroughSmp() {
		return this.smp.getCtroughSmpInfo();
	}
	
	/**
	 * GUIController constructor can create a GUIController  
	 */
	public GUIController() {
		// TODO Auto-generated constructor stub
	}

}
