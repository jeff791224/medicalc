/**
* This class implements the vancomycin pharmakinetic method for concentration recommendation
* for pharmacy calculation.
* 
* @author  JeffShih
* @version 1.0
* @since   2017-06-23 
*/


public class VancomycinPK extends PK{

	
	private double cldrug;
	private double vd;
	private double k;
	private double thalf;// = Math.log(2) / k;
	private double tpeak;//tif
	private double ttrough;//tau
	private double md;
	private double cpeak;//(md / vd) / (1 - Math.Exp(-k * tau)) * Math.Exp(-k * tpeak);
	private double ctrough;//(md / vd) / (1 - Math.Exp(-k * tau)) * Math.Exp(-k * ttrough);
	
	
	
	public VancomycinPK(MedicalRecord mr,Recommendation rmt,Regimen regi) {
		/**
		 * this is the constructor of Vancomycin PK calculation class
		 * @param mr is the patients medical record
		 * @param rmt is the recommendation class for data passing
		 * @param regi is the regimen class for data passing
		*/
		super(mr,rmt,regi);
		this.setcldrug(mr, rmt);
		this.setvd(mr, rmt);
		this.setk(mr, rmt);
		this.md = regi.md;
		this.tpeak = regi.tif;
		this.thalf = Math.log(2)/k;
		this.ttrough = regi.tau;
		this.cpeak = (regi.md / this.vd) / (1 - Math.exp(-k * regi.tau)) * Math.exp(-k * tpeak);
		this.ctrough = (regi.md / this.vd) / (1 - Math.exp(-k * regi.tau)) * Math.exp(-k * ttrough);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getcldrug() {
		/**
		 * this function return the value of cldrug
		 * @return a double indicate cldrug.
		 * 
		*/
		return cldrug;
	}

	@Override
	public double getVd() {
		/**
		 * this function return the value of VD
		 * @return a double indicate VD.
		 * 
		*/
		return vd;
	}

	@Override
	public double getK() {
		/**
		 * this function return the value of K
		 * @return a double indicate K.
		 * 
		*/
		return k;
	}

	@Override
	public double getThalf() {
		/**
		 * this function return the value of T halflife
		 * @return a double indicate T halflife.
		 * 
		*/
		return thalf;
	}

	@Override
	public double getCpeak() {
		/**
		 * this function return the value of peak concerntration
		 * @return a double indicate peak concerntration.
		 * 
		*/
		return cpeak;
	}

	@Override
	public double getCtrough() {
		/**
		 * this function return the value of trough concerntration
		 * @return a double indicate trough concerntation.
		 * 
		*/
		return ctrough;
	}

	@Override
	public void setcldrug(MedicalRecord mr,Recommendation rmt) {
		/**
		 * this function calculate the value of cldrug
		 * @param MedicalRecord mr and rmt for the data needed.
		 * 
		*/
		this.cldrug = (0.695 * mr.getClcr()/mr.getWt() + 0.05) * mr.getWt()* 60.0 / 1000.0;
	}

	@Override
	public void setvd(MedicalRecord mr,Recommendation rmt) {
		/**
		 * this function calculate the value of vd
		 * @param MedicalRecord mr and rmt for the data needed.
		 * 
		*/
		this.vd = mr.getOB() ? 0.7 * mr.getIBW()* 30.0 / 25.0 : 0.7 * mr.getWt(); 
	}

	@Override
	public void setk(MedicalRecord mr,Recommendation rmt) {
		/**
		 * this function calculate the value of K
		 * @param MedicalRecord mr and rmt for the data needed.
		 * 
		*/
		this.k = this.cldrug/this.vd;
	}

}
