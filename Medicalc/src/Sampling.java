/**
* This class implements the sampling method for concentration calculation.
* the data is obtained by vein puncturing. 
* 
* @author  JeffShih
* @version 1.0
* @since   2017-06-23 
*/

public class Sampling {

	
	private double k;
	private double Thalf;
	private double Vd;
	private double Cldrug;
	private double Ctrough;
	private double Cpeak;
	public double c1;
	public double c2;
	public double t1;
	public double t2;
	public double md;
	public double tau;
	public double tif;
	
	public Sampling() {
		/**
		 * this is the default constructor
		 */
		// TODO Auto-generated constructor stub
	}
	
	public void setSmp(double[] param){
		/**
		 * this function set the parameter of regimen
		 * @param double[] for seting the parameter
		 * 
		*/
		this.c1 = param[0];
		this.c2 = param[1];
		this.t1 = param[2];
		this.t2 = param[3];
		this.md = param[4];
		this.tau = param[5];
		this.tif = param[6];
		
		this.k = getKSmp(t1,c1,t2,c2);
		this.Thalf = getThalfSmp(t1,c1,t2,c2);
		this.Vd  = getVdSmp(md,tau,t1,c1,t2,c2);
		this.Cldrug = getCldrugSmp(md,tau,t1,c1,t2,c2);
		this.Cpeak = getCpeakSmp(md,tau,tif,t1,c1,t2,c2);
		this.Ctrough = getCtroughSmp(md,tau,tif,t1,c1,t2,c2);
	}
	
	
	public double getKSmp(double t1, double c1, double t2, double c2) {
		/**
		 * this function calculate the value of k by sampling method
		 * @param double t1,t2 for the time interval
		 * @param double c1,c2 for the blood concerntration.
		 * @return a double indicate k
		 * 
		*/
		double k = - Math.log(c1 / c2) / (t1 - t2);
		return k;
	}
	
	public double getThalfSmp(double t1, double c1, double t2, double c2) {
		/**
		 * this function calculate the value of t halflife by sampling method
		 * @param double t1,t2 for the time interval
		 * @param double c1,c2 for the blood concerntration.
		 * @return a double indicate halflife
		 * 
		*/
		double thalf = Math.log(2) / getKSmp(t1, c1, t2, c2);
		return thalf;
	}
	
	public double getVdSmp(double md, double tau, double t1, double c1, double t2, double c2) {
		/**
		 * this function calculate the value of vd by sampling method
		 * @param md for the mean dose
		 * @param tau for the parameter tau
		 * @param double t1,t2 for the time interval
		 * @param double c1,c2 for the blood concerntration.
		 * @return a double indicate vd
		 * 
		*/
		double vd = md / (1 - Math.exp(-getKSmp(t1, c1, t2, c2) * tau)) * Math.exp(-getKSmp(t1, c1, t2, c2) * t1) / c1;
		return vd;
	}
	
	public double getCldrugSmp(double md, double tau, double t1, double c1, double t2, double c2) {
		/**
		 * this function calculate the value of Cldrug by sampling method
		 * @param md for the mean dose
		 * @param tau for the parameter tau
		 * @param double t1,t2 for the time interval
		 * @param double c1,c2 for the blood concerntration.
		 * @return a double indicate Cldrug
		 * 
		*/
		double cldrug = getKSmp(t1, c1, t2, c2) * getVdSmp(md, tau, t1, c1, t2, c2); 
		return cldrug;
	}
	
	public String getKSmpInfo() {
		/**
		 * this function return the value of K
		 * @return a double indicate K.
		 * 
		*/
		return Double.toString(this.k);
	}
	
	public String getThalfSmpInfo() {
		/**
		 * this function return the value of halflife time
		 * @return a double indicate halflife time.
		 * 
		*/
		return Double.toString(this.Thalf);
	}
	
	public String getVdSmpInto() {
		/**
		 * this function return the value of vd
		 * @return a double indicate vd.
		 * 
		*/
		return Double.toString(this.Vd);
	}
	
	public String getCldrugSmpInfo() {
		/**
		 * this function return the value of Cldrug
		 * @return a double indicate Cldrug.
		 * 
		*/
		return Double.toString(this.Cldrug);
	}

	public double getCpeakSmp(double md, double tau, double tif, double t1, double c1, double t2, double c2) {
		/**
		 * this function calculate the value of peak concerntration by sampling method
		 * @param md for the mean dose
		 * @param tau for the parameter tau
		 * @param double t1,t2 for the time interval
		 * @param double c1,c2 for the blood concerntration.
		 * @return a double indicate peak concerntration
		 * 
		*/
		double tpeak = tif;
		double cpeak = (md / getVdSmp(md, tau, t1, c1, t2, c2)) / (1 - Math.exp(-getKSmp(t1, c1, t2, c2) * tau)) * Math.exp(-getKSmp(t1, c1, t2, c2) * tpeak);
		return cpeak;
	}

	public double getCtroughSmp(double md, double tau, double tif, double t1, double c1, double t2, double c2) {
		/**
		 * this function calculate the value of trough concerntration by sampling method
		 * @param md for the mean dose
		 * @param tau for the parameter tau
		 * @param double t1,t2 for the time interval
		 * @param double c1,c2 for the blood concerntration.
		 * @return a double indicate trough concerntration.
		 * 
		*/
		double ttrough = tau;
		double ctrough = (md / getVdSmp(md, tau, t1, c1, t2, c2)) / (1 - Math.exp(-getKSmp(t1, c1, t2, c2) * tau)) * Math.exp(-getKSmp(t1, c1, t2, c2) * ttrough);
		return ctrough;
	}
	
	public String getCpeakSmpInfo() {
		/**
		 * this function return the value of peak concerntration
		 * @return a double indicate peak concerntration.
		 * 
		*/
		return Double.toString(this.Cpeak);
	}
	
	public String getCtroughSmpInfo() {
		/**
		 * this function return the value of trough concerntration
		 * @return a double indicate trough concerntation.
		 * 
		*/
		return Double.toString(this.Ctrough);
	}
	
	

	
	
}
