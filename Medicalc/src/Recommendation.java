/**
* This is the abstract class implements emprical and specific method for concentration recommendation
* for pharmacy calculation.
* 
* @author  JeffShih
* @version 1.0
* @since   2017-06-23 
*/

public abstract class Recommendation {

	private double dwt;
	private double content; // mg/vial
	private double ld0_min;
	private double ld0_max;
	private double md0_min;
	private double md0_max;
	private int tau0_min;
	private int tau0_max;
	private int tif0_min;
	private int tif0_max;
	private double ld1_min;
	private double ld1_max;
	private double md1_min;
	private double md1_max;
	private int tau1_min;
	private int tau1_max;
	private int tif1_min;
	private int tif1_max;
	public double abw;
	
	public Recommendation(MedicalRecord mr) {
	/**
	 * This is the default constructor
	 */
	}

	public double getAminoglycosideMDAdj(double clcr,int  tau0_min){
		/**
		 * This function implement the calculation of Aminoglycoside parameter for adjusting
		 * specific drug dosing.
		 * @param clcr is the creatinine clear rate
		 * @param tau0_min is the minimal clearing time.
		 * @return a double for the ratio of adjusting specific drug dosing.
		 */
		double[] mdAdj_clcr = { 90.0, 80.0, 70.0, 60.0, 50.0, 40.0, 30.0, 25.0, 20.0, 17.0, 15.0, 12.0, 10.0, 7.0, 5.0, 2.0, 0.0 };
		double[] mdAdj_Q8H = { 0.84, 0.80, 0.76, 0.71, 0.65, 0.57, 0.48, 0.43, 0.37, 0.33, 0.31, 0.27, 0.24, 0.19, 0.16, 0.11, 0.08 };
		double[] mdAdj_Q12H = { 1.0, 0.91, 0.88, 0.84, 0.79, 0.72, 0.63, 0.57, 0.50, 0.46, 0.42, 0.37, 0.34, 0.28, 0.23, 0.16, 0.11 };
		double[] mdAdj_Q24H = { 1.0, 1.0, 1.0, 1.0, 1.0, 0.92, 0.86, 0.81, 0.75, 0.70, 0.67, 0.61, 0.56, 0.47, 0.41, 0.30, 0.21 };
		
		double mdAdj = 0.0;
		
		if (clcr >= 90.0) {
			mdAdj = 1;
		} else if (clcr == 0.0) {
			switch (tau0_min) {
				case 8:
					mdAdj = mdAdj_Q8H[16]; 
					break;
				case 12:
					mdAdj = mdAdj_Q12H[16]; 
					break;
				case 24:
					mdAdj = mdAdj_Q24H[16]; 
					break;
			}
		} else {
			for (int i = 0; i < mdAdj_clcr.length - 1; i++) {
				if (clcr < mdAdj_clcr[i] && clcr >= mdAdj_clcr[i + 1]) {
					switch (tau0_min) {
					case 8:
						mdAdj = mdAdj_Q8H[i]; 
						break;
					case 12:
						mdAdj = mdAdj_Q12H[i]; 
						break;
					case 24:
						mdAdj = mdAdj_Q24H[i]; 
						break;
					}
				}
			}
		}
		
		
		return mdAdj;
	};	
	public abstract String getld0info();
	public abstract String getmd0info();
	public abstract String gettif0info();
	public abstract String gettau0info();
	public abstract String getld1info();
	public abstract String getmd1info();
	public abstract String gettau1info();
	public abstract String gettif1info();
}


