/**
* This class implements the Gentamicin recommendation method for concentration recommendation
* for pharmacy calculation.
* 
* @author  JeffShih
* @version 1.0
* @since   2017-06-23 
*/
public class GentamicinRcmd extends Recommendation{


	private double dwt;
	private double content; // mg/vial
	private double ld0_min;
	private double ld0_max;
	private double md0_min;
	private double md0_max;
	private int tau0_min;
	private int tau0_max;
	private double tif0_min;
	private double tif0_max;
	private double ld1_min;
	private double ld1_max;
	private double md1_min;
	private double md1_max;
	private int tau1_min;
	private int tau1_max;
	private double tif1_min;
	private double tif1_max;
	public double abw;
	
	
	public double getAminoglycosideMDAdj(double clcr,int  tau0_min){
		/**
		 * This function implement the calculation of Aminoglycoside parameter for adjusting
		 * specific drug dosing.
		 * @param clcr is the creatinine clear rate
		 * @param tau0_min is the minimal clearing time.
		 * @return a double for the ratio of adjusting specific drug dosing.
		 */
		double[] mdAdj_clcr = { 90.0, 80.0, 70.0, 60.0, 50.0, 40.0, 30.0, 25.0, 20.0, 17.0, 15.0, 12.0, 10.0, 7.0, 5.0, 2.0, 0.0 };
		double[] mdAdj_Q8H = { 0.84, 0.80, 0.76, 0.71, 0.65, 0.57, 0.48, 0.43, 0.37, 0.33, 0.31, 0.27, 0.24, 0.19, 0.16, 0.11, 0.08 };
		double[] mdAdj_Q12H = { 1.0, 0.91, 0.88, 0.84, 0.79, 0.72, 0.63, 0.57, 0.50, 0.46, 0.42, 0.37, 0.34, 0.28, 0.23, 0.16, 0.11 };
		double[] mdAdj_Q24H = { 1.0, 1.0, 1.0, 1.0, 1.0, 0.92, 0.86, 0.81, 0.75, 0.70, 0.67, 0.61, 0.56, 0.47, 0.41, 0.30, 0.21 };
		
		double mdAdj = 0.0;
		
		if (clcr >= 90.0) {
			mdAdj = 1;
		} else if (clcr == 0.0) {
			switch (tau0_min) {
				case 8:
					mdAdj = mdAdj_Q8H[16]; 
					break;
				case 12:
					mdAdj = mdAdj_Q12H[16]; 
					break;
				case 24:
					mdAdj = mdAdj_Q24H[16]; 
					break;
			}
		} else {
			for (int i = 0; i < mdAdj_clcr.length - 1; i++) {
				if (clcr < mdAdj_clcr[i] && clcr >= mdAdj_clcr[i + 1]) {
					switch (tau0_min) {
					case 8:
						mdAdj = mdAdj_Q8H[i]; 
						break;
					case 12:
						mdAdj = mdAdj_Q12H[i]; 
						break;
					case 24:
						mdAdj = mdAdj_Q24H[i]; 
						break;
					}
				}
			}
		}
		
		
		return mdAdj;
	};	
	
	public GentamicinRcmd(MedicalRecord mr) {
		/**
		 * this is the constructor of Gentamicin recommendation calculation class
		 * @param mr is the patients medical record
		*/
		super(mr);
		this.content = 80.0;
		double ibw = mr.getIBW();
		double wt = mr.getWt();
		boolean ob = mr.getOB();
		double clcr = mr.getClcr();
		this.dwt = ibw; 
		double abw = (ibw + 0.4 * (wt - ibw)); // abw, adjust body weght 
		// gentamicin empirical dosing method
		this.ld0_min = ob ? 1.5 * abw : 1.5 * dwt; 
		this.ld0_max = ob ? 2.0 * abw : 2.0 * dwt;
		if(clcr >= 90.0)
			tau0_min = 8;
		else if(clcr < 90.0 && clcr >= 40) 
			tau0_min = 12;
		else if(clcr < 40.0)
			tau0_min = 24;
		this.tau0_max = tau0_min;
		double mdAdj = getAminoglycosideMDAdj(clcr, tau0_min);
		md0_min = ld0_min * mdAdj / (24.0 / tau0_max); 
		md0_max = ld0_max * mdAdj / (24.0 / tau0_min); 
		tif0_min = Math.round(md0_min / content * 0.5);
		tif0_max = Math.round(md0_max / content * 2);
		// gentamicin ODD method, once daily dosing
		md1_min = ob ? 5.0 * abw : 5.0 * dwt; 
		md1_max = ob ? 7.0 * abw : 7.0 * dwt; 
		if(clcr >= 20.0) 
			tau1_min = 24;
		else
			tau1_min = 48;
		tau1_max = tau1_min;
		tif1_min = Math.round(md1_min / content * 0.5);
		tif1_max = Math.round(md1_max / content * 2.0);
		
	}

	

	public String getld0info(){
		/**
		 * this function return the value of ld by emprical method
		 * @return a string indicate ld by emprical method.
		 * 
		*/
		if(ld0_min == ld0_max)
			return String.valueOf(ld0_min);
		else
			return String.valueOf(ld0_min)+"~"+String.valueOf(ld0_max);
	}
	
	public String getmd0info(){
		/**
		 * this function return the value of md by emprical method
		 * @return a string indicate md by emprical method.
		 * 
		*/
		if(md0_min == md0_max)
			return String.valueOf(md0_min);
		else
			return String.valueOf(md0_min)+"~"+String.valueOf(md0_max);
		
	}

	public String gettau0info(){
		/**
		 * this function return the value of tau by emprical method
		 * @return a string indicate tau by emprical method.
		 * 
		*/
		if(tau0_min == tau0_max)
			return String.valueOf(tau0_min);
		else
			return String.valueOf(tau0_min)+"~"+String.valueOf(tau0_max);
		
	}

	public String gettif0info(){
		/**
		 * this function return the value of tif by emprical method
		 * @return a string indicate tif by emprical method.
		 * 
		*/
		if(tif0_min == tif0_max)
			return String.valueOf(tif0_min);
		else
			return String.valueOf(tif0_min)+"~"+String.valueOf(tif0_max);
		
	}

	public String getld1info(){
		/**
		 * this function return the value of ld by oce daily method
		 * @return a string indicate ld by oce daily method.
		 * 
		*/
		if(ld1_min == ld1_max)
			return String.valueOf(ld1_min);
		else
			return String.valueOf(ld1_min)+"~"+String.valueOf(ld1_max);
		
	}
	
	public String getmd1info(){
		/**
		 * this function return the value of md by oce daily method
		 * @return a string indicate md by oce daily method.
		 * 
		*/
		if(md1_min == md1_max)
			return String.valueOf(md1_min);
		else
			return String.valueOf(md1_min)+"~"+String.valueOf(md1_max);
		
	}

	public String gettau1info(){
		/**
		 * this function return the value of tau by oce daily method
		 * @return a string indicate tau by oce daily method.
		 * 
		*/
		if(tau1_min == tau1_max)
			return String.valueOf(tau1_min);
		else
			return String.valueOf(tau1_min)+"~"+String.valueOf(tau1_max);
		
	}

	public String gettif1info(){
		/**
		 * this function return the value of tif by oce daily method
		 * @return a string indicate tif by oce daily method.
		 * 
		*/
		if(tif1_min == tif1_max)
			return String.valueOf(tif1_min);
		else
			return String.valueOf(tif1_min)+"~"+String.valueOf(tif1_max);
		
	}

}
