/**
* This class implements the drug factory patient drug recipe.
* set the specific drug for each patient
* 
* @author  JeffShih
* @version 1.0
* @since   2017-06-23 
*/

public class Drugs {
	
	


	
	   public static String[] TradeName = {
			"vancomycin 500 mg/vial",
			"gentamicin 80 mg/vial", 
			"amikacin 500 mg /vial"
		};
		
		public static String[] GenericName = {
			"vancomycin",
			"gentamicin", 
			"amikacin"
		};
		
		public static String[] OtherDosingMethod = {
			"Rodvold's Method",
			"Oce Daily Dosing",
			"Oce Daily Dosing"
		};
	
	public enum Type{Vancomycin,Gentamicin,Amikacin}
	

	
	public PK setDrug(Drugs.Type drug,MedicalRecord mr,Recommendation rmd,Regimen regi){
		/**
		 * this function is the factory for drug
		 * @param Drugs.Type drug is the drug name
		 * @param MedicalRecord mr is the patient's personal information
		 * @param rmd is the recommendation class for data passing
		 * @param regi is the regimen class for data passing
		 * @return PK class for pharmacokinetics calculation
		*/
		switch(drug){
		case Vancomycin:
			return new VancomycinPK(mr,rmd,regi);
		case Gentamicin:
			return new GentamicinPK(mr,rmd,regi);
		case Amikacin:
			return new AmikacinPK(mr,rmd,regi);
		default:
			return null;
	}	
	}
	
	
	

}
