/**
* This class implements the Recomnendation factory for patient drug recipe.
* set the specific drug for each patient and its recommendation method.
* 
* @author  JeffShih
* @version 1.0
* @since   2017-06-23 
*/

public class Recommendations {

	public Recommendations() {}

	
	
	public Recommendation setDrug(Drugs.Type drug,MedicalRecord mr){
		/**
		 * this function is the factory for drug
		 * @param Drugs.Type drug is the drug name
		 * @param MedicalRecord mr is the patient's personal information
		 * @return Recommendation class for specific method for calculating the recommended dosing.
		*/
		switch(drug){
		case Vancomycin:
			return new VancomycinRcmd(mr);
		case Gentamicin:
			return new GentamicinRcmd(mr);
		case Amikacin:
			return new AmikacinRcmd(mr);
		default:
			return null;
	}	
	}
}
