import java.sql.*;


public class MedicalRecord {

	private int ptid = 00000;
	private String sex = "M";
	private double ht = 0;
	private double wt = 0;
	private int age = 0;
	private double scr = 0;
	private double ibw = 0;
	private boolean obe = false;
	//private String[] record = new String[8];
	private double clcr = 0;
	private double egfr = 0;
	private String name = "";
	public MedicalRecord() {
		// TODO Auto-generated constructor stub
	}
	
	public void setPatientRecord(String[] param){
		this.ptid = Integer.parseInt(param[0]);
		this.sex = param[1];
		this.ht = Double.parseDouble(param[2]);
		this.wt = Double.parseDouble(param[3]);
		this.age = Integer.parseInt(param[4]);
		this.scr = Double.parseDouble(param[5]);
		this.name = param[6];
		switch(sex){
		case "M":
			this.ibw = (ht-80)*0.7;
			break;
		case "F":
			this.ibw = (ht-70)*0.6;
			break;
		}
		this.obe = (wt > ibw*1.3);
		if(wt<ibw){
			this.clcr = (140.0 - age)/0.8;
		}else{
			this.clcr = (140.0-age)/this.scr;};
		this.egfr = 186.0 * Math.pow(scr, -0.999) * Math.pow(age, -0.203);
		
		if(this.sex == "F"){
			this.clcr*=0.85;
			this.egfr*=0.762;
		}
		
		
		try{
		//Class.forName ("oracle.jdbc.OracleDriver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/Medicalc","jeff","jeff");
		Statement mystm = conn.createStatement();
	
		int rs = mystm.executeUpdate("Insert Into MedicalRecord (ptid,Gender,ht,wt,Age,SCR,obe,IBW,CLCR,EGFR,name) "
				+ "VALUES ("+this.ptid+","+"\""+this.sex + "\"," + this.ht+ "," +this.wt+ "," +this.age+ "," +this.scr+ ","
				+ this.obe+ "," +this.ibw+ "," +this.clcr+ "," +this.egfr + "," +"\""+ this.name +"\")");
		
		
		}catch(Exception exc){
			exc.printStackTrace();
			}
		
		
	}
	
	public void queryPatientRecord(int ptid){
		Connection conn;
		this.ptid = ptid;
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/Medicalc","jeff","jeff");
			Statement mystm = conn.createStatement();
			ResultSet rs = mystm.executeQuery("Select * from MedicalRecord where ptid = " + ptid);
			rs.next();
			this.sex = rs.getString(3);
			this.ht = rs.getDouble(4);
			this.wt = rs.getDouble(5);
			this.age = rs.getInt(6);
			this.scr = rs.getInt(7);
			this.obe = rs.getBoolean(8);
			this.ibw = rs.getDouble(9);
			this.clcr= rs.getDouble(10);
			this.egfr= rs.getDouble(11);
			this.name=rs.getString(12);
		} catch (Exception exc) {
						// TODO Auto-generated catch block
			exc.printStackTrace();
		}
		
				
	}	

	public double getScr(){
		return this.scr;
	}
	
	public double getHt(){
		return this.ht;
	}
		
	public String getName(){
		return this.name;
	}
	
	public String getSex(){
		return this.sex;
	}
	
	public int getAge(){
		return this.age;
	}
	
	public boolean getOB(){
		return this.obe;
	}
	
	public double getIBW(){
		return this.ibw;
		
	}
	
	public double getEGFR(){
		return this.egfr;
	}
	
	public double getClcr(){
		return this.clcr;
	}
	
	public double getWt(){
		return this.wt;
	}

}
